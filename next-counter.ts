export function nextString(value: string, encoding: string = "hex") {
  const buffer = Buffer.from(value, encoding);
  const nbuffer = nextBuffer(buffer)
  return nbuffer.toString(encoding)
}

export function nextBuffer(buffer) {
  const array = [ ...buffer ];
  array[array.length - 1]++;
  normalize(array)
  return Buffer.from(array)
}


export function normalize(array) {
  for (let i = array.length - 1; i > 0; i--) {
    while (array[i] > 255) {
      array[i]   = array[i]   - 256;
      array[i-1] = array[i-1] + 1;
    }
  }
}

export function enshureBuffer(value: string|Buffer, encoding: string = "hex") {
  if (typeof value === "string") {
    return Buffer.from(value, encoding);
  } else {
    return value;
  }
}

