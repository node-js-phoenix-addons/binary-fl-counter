import { enshureBuffer, normalize } from "./next-counter";

export class Counter {

  private current: number[] = []

  constructor (public length: number) {
    while (this.current.length < length) this.current.push(0);
  }

  /**
   * Get current value
   *
   * @param {string} encoding
   */
  public get (encoding: string = "hex"): string {
    normalize(this.current);
    return Buffer.from(this.current).toString(encoding)
  }

  public set (value: string|Buffer, encoding: string = "hex") {
    const buffer = enshureBuffer(value, encoding);
    if (buffer.length !== this.length) {
      throw new Error("Icorrect length");
    }
    this.current = [ ...buffer ];
  }

  public inc() {
    this.current[this.length-1]++;
    return this.get()
  }

}

